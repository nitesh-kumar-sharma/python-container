#!/bin/bash
echo "Alpine v3.9"
echo "TimeZone" $(date +'%:z %Z')
echo "python version" 
echo $(python3 --version)

if [[ -z $1 ]]; then
  echo "docker container started in continous running mode"
  echo "you can kill this terminal if you wish and can use docker exec -it <container_id> bash "
  echo "anytime."
  while true; do sleep 1000; done
else 
	exec $@;
fi;